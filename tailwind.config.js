module.exports = {
  purge: [
    "./components/**/*.{vue,js}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      textColor: {
        primary: "#4A5568",
        primary1: "#718096",
        primary2: "#2D3748",
        primary3: "#319795",
      },
      colors: {
        brand: {
          green: "#319795",
          blue: "#3182CE",
          darkGray: "#4A5568",
          lightGray: "#CBD5E0",
          gray: "#718096",
          lightestGreen: "#E6FFFA",
          lightGreen: "#81E6D9",
          lightestBlue: "#EBF4FF",
        },
      },
      fontFamily: {
        lato: ["Lato"],
      },
      fontSize: {
        font1: [
          "42px",
          {
            lineHeight: "50px",
          },
        ],
        font2: [
          "14px",
          {
            lineHeight: "17px",
          },
        ],
        font3: [
          "14px",
          {
            lineHeight: "17px",
          },
        ],
        font4: [
          "21px",
          {
            lineHeight: "25px",
          },
        ],
        font5: [
          "130px",
          {
            lineHeight: "156px",
          },
        ],
        font6: [
          "15.75px",
          {
            lineHeight: "19px",
          },
        ],
        font7: [
          "130px",
          {
            lineHeight: "156px",
          },
        ],
      },

      height: {
        98: "33rem",
        4: "40px",
      },
      spacing: {
        7: "319.97px",
        8: "17px",
      },
      letterSpacing: {
        normal: "0.84px",
        snormal: "1.26px",
        nnormal: "0.47px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
